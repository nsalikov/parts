# -*- coding: utf-8 -*-
import re
import json
import itertools
from bs4 import BeautifulSoup
import scrapy


class AsusServersSpider(scrapy.Spider):
    name = 'asus_servers'
    allowed_domains = ['asus.com']
    start_urls = [
        # https://www.asus.com/us/Commercial-Servers-Workstations/Commercial-Servers-Products/
        'https://www.asus.com/us/OfficialSiteAPI.asmx/GetModelResults?WebsiteId=52&ProductLevel2Id=4544&FiltersCategory=10074&Filters=&Sort=3&PageNumber=1&PageSize=100',
        # https://www.asus.com/us/Commercial-Servers-Workstations/Commercial-Server-Motherboards-Products/
        'https://www.asus.com/us/OfficialSiteAPI.asmx/GetModelResults?WebsiteId=52&ProductLevel2Id=4544&FiltersCategory=10151&Filters=&Sort=3&PageNumber=1&PageSize=100',
    ]


    def parse(self, response):
        try:
            data = json.loads(response.text)
        except ValueError as e:
            self.logger.warning('Unable to parse json: {}'.format(response.text))
            return

        for d in data['Result']['Obj']:
            url = d['Url'].rstrip(' /') + '/specifications/'
            yield response.follow(url, callback=self.parse_item)


    def parse_item(self, response):
        d = {}

        d['url'] = response.url
        d['name'] = response.css('span#ctl00_ContentPlaceHolder1_ctl00_span_model_name ::text').extract_first()
        d['manufactuer'] = 'ASUS'
        d['series'] = None
        d['sku'] = None

        images = [response.urljoin(url) for url in re.findall("product_image_url = '(.+?)'", response.text) if url]
        if images:
            d['image'] = images[0]
        else:
            d['image'] = None

        d['specs'] = self.get_specs(response)
        d['compatible_parts'] = []

        return d


    def get_specs(self, response):
        spec_rows = 'ul.product-spec>li'
        d = {}

        for li in response.css(spec_rows):
            name = li.css('span.spec-item ::text').extract_first()

            data = [line.strip() for line in li.xpath('child::div[@class="spec-data"]/node()').extract()]
            # data = [line for line in data if not line.startswith('*')]
            data = [list(v) for k,v in itertools.groupby(data, key=lambda line: re.search('<\s*br.*?>', line, re.IGNORECASE)) if not k]
            data = [' '.join(line) for line in data]

            for i, line in enumerate(data):
                match = re.search('(<strong>.+?</strong>)(.+)', line)
                if match:
                    data[i:i+1] = (match.group(1).strip(), match.group(2).strip())

            data = [self.get_text(line) for line in data]
            data = self.groupby(data)
            data = self.list2dict(data)

            d[name] = data

        return d


    def get_text(self, html):
        soup = BeautifulSoup(html, 'lxml')

        for tag in soup.find_all('sup'):
            tag.decompose()

        text = soup.get_text(strip=True)

        return text


    def groupby(self, l):
        m = []

        colon = False

        for e in l:
            if e.endswith(':'):
                colon = True
                m.append([e.strip(' :')])
            elif not colon:
                m.append(e)
            else:
                m[-1].append(e)

        return m


    def list2dict(self, l):
        m = []

        for e in l:
            if type(e) == list:
                d = {e[0]: e[1:]}
                m.append(d)
            else:
                m.append(e)

        return m
