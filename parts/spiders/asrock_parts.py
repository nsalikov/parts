# -*- coding: utf-8 -*-
import re
import scrapy
import itertools
from bs4 import BeautifulSoup
from scrapy.exceptions import CloseSpider
from urllib.parse import urlparse, urlunparse, parse_qsl, urlencode, unquote, quote


class AsrockPartsSpider(scrapy.Spider):
    name = 'asrock_parts'
    allowed_domains = ['asrockrack.com']
    start_urls = [
        'https://www.asrockrack.com/general/products-ajax.asp?Model=&Type=Accessories&CPU=&Socket=&DIMM=&Form=&Form2=&Acc=&p=1',
    ]

    seen = set()


    def parse(self, response):
        items_css = 'div.ProductsItems>a::attr(href)'
        items = response.css(items_css).extract()
        items = [item.strip() for item in items if item.startswith('productdetail') and item not in self.seen]

        if not items:
            return

        for item in items:
            self.seen.add(item)

            yield response.follow(item, callback=self.parse_item)

        next_page = self.get_next_page(response)
        if next_page:
            yield response.follow(next_page, callback=self.parse)


    def parse_item(self, response):
        d = {}

        url = response.request.url
        d['url'] = url

        match = re.search('Model=(.+)', url)
        if match:
            model = match.group(1)
        else:
            return

        d['name'] = response.css('h1 ::text').extract_first()
        if d['name']:
            d['name'] = d['name'].strip()

        d['manufactuer'] = 'ASRock'
        d['series'] = None
        d['sku'] = unquote(model)

        d['image'] = response.css('a.jqzoom ::attr(href)').extract_first()
        if d['image']:
            d['image'] = response.urljoin(d['image'])

        d['specs'] = []
        d['compatible_servers'] = []

        specs_url = 'https://www.asrockrack.com/general/productdetail-ajax.asp?Model={}&cat=Specifications'.format(quote(d['sku']))
        return response.follow(specs_url, callback=self.parse_specs, meta={'item': d})


    def parse_specs(self, response):
        d = response.meta['item']
        d['specs'] = self.get_specs(response)

        return d


    def get_next_page(self, response):
        request_url = response.request.url
        next_url = None

        p = list(urlparse(request_url))
        q = dict(parse_qsl(p[4]))

        try:
            q['p'] = int(q['p']) + 1
            p[4] = urlencode(q)
            next_url = urlunparse(p)
        except:
            self.logger.warning('Could not parse url. Something may be wrong with <{}>'.format(request_url))

        return next_url


    def get_specs(self, response):
        rows = response.css('table.ListSpec>tr')
        rows = self.groupby(rows)
        rows = self.list2dict(rows)

        return rows


    def groupby(self, rows):
        m = []

        color = False

        for row in rows:
            if row.css('tr[bgcolor="#dddddd"]'):
                color = True
                text = row.css('tr>td ::text').extract_first()
                text = text.replace('\xa0', '')
                m.append([text])
            elif not color:
                text = list(filter(None, [t.strip() for t in row.css('tr>td ::text').extract()]))
                text = ' '.join(text)
                text = text.replace('\xa0', '')

                m.append(text)
            else:
                name = row.css('tr>td:nth-child(1) ::text').extract_first()
                data = row.xpath('child::td[2]/node()').extract()
                data = [list(v) for k,v in itertools.groupby(data, key=lambda line: re.search('<\s*br.*?>', line, re.IGNORECASE)) if not k]
                data = [' '.join(line) for line in data]
                data = [self.get_text(line) for line in data]
                data = [line.lstrip(' -') for line in data]

                data = [name] + data

                m[-1].append(data)

        return m


    def get_text(self, html):
        soup = BeautifulSoup(html, 'lxml')

        for tag in soup.find_all('sup'):
            tag.decompose()

        text = soup.get_text()
        text = re.sub('[\xa0 ]+', ' ', text)
        text = text.strip()

        return text


    def list2dict(self, lst):
        d = {}

        for l in lst:
            header = l[0]
            tail = l[1:]

            if header not in d:
                d[header] = {}

            for t in tail:
                name = t[0]
                rest = t[1:]

                if name not in d[header]:
                    d[header][name] = []

                d[header][name] = rest

        return d
