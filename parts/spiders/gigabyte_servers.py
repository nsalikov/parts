# -*- coding: utf-8 -*-
import re
import json
import scrapy
import itertools
from bs4 import BeautifulSoup


class GigabyteServersSpider(scrapy.Spider):
    name = 'gigabyte_servers'
    allowed_domains = ['gigabyte.com']
    start_urls = [
        '101', # Server Motherboard
        '102', # Rack Server
        '108', # Tower Server
        '106', # High Performance Computing System
        '107', # Density Optimized
        '110', # ARM
        '103', # Storage Server
        '113', # Overclocking Servers
        # '112', # Embedded Computing
        # '109', # Accessory
    ]


    def start_requests(self):
        for ck in self.start_urls:
            # url = 'https://b2b.gigabyte.com/Ajax/Product/GetListInfoByAjax'
            url = 'https://www.gigabyte.com/Ajax/Product/GetListInfoByAjax'

            headers = {
                'Accept-Encoding': 'gzip',
                'Accept-Language': 'en-US',
                'Accept': 'application/json, text/javascript, */*; q=0.01',
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                'Origin': 'https://www.gigabyte.com',
                'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36',
                'X-Requested-With': 'XMLHttpRequest',
            }

            body = 'ck={}&f=0&page=1'.format(ck)

            yield scrapy.Request(url, method='POST', headers=headers, body=body, meta={'ck': ck}, callback=self.parse, dont_filter=True)


    def parse(self, response):
        try:
            data = json.loads(response.text)
        except ValueError as e:
            self.logger.warning('Unable to parse json at <{}>: {}'.format(response.url, response.text))
            return

        items = data['ModelList']

        for item in items:
            _id = item['MainInfo']['seq_product']
            url = 'https://www.gigabyte.com/api/ProductSpec/{}'.format(_id)

            headers = {
                'Accept-Encoding': 'gzip',
                'Accept-Language': 'en-US,en;q=0.9,ru-RU;q=0.8,ru;q=0.7,ja;q=0.6,pt;q=0.5',
                'Accept': 'application/json, text/javascript, */*; q=0.01',
                'Referer': 'https://www.gigabyte.com/',
                'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36',
                'X-Requested-With': 'XMLHttpRequest',
            }

            d = {}

            d['url'] = response.urljoin(item['MainInfo']['UrlCountryCodeModel'])
            d['name'] = item['MainInfo']['model_name']
            d['sku'] = d['name']
            d['manufactuer'] = 'Gigabyte'
            d['series'] = None

            d['image'] = item['ListPageImageUrl']
            if d['image'].startswith('//'):
                d['image'] = 'https:' + d['image']

            d['compatible_parts'] = []

            yield scrapy.Request(url, headers=headers, callback=self.parse_item, meta={'item': d})

        CurrentPage = data['CurrentPage']
        MaximumPage = data['MaximumPage']

        if CurrentPage < MaximumPage:
            page = CurrentPage + 1
            ck = response.meta['ck']

            # url = 'https://b2b.gigabyte.com/Ajax/Product/GetListInfoByAjax'
            url = 'https://www.gigabyte.com/Ajax/Product/GetListInfoByAjax'

            headers = {
                'Accept-Encoding': 'gzip',
                'Accept-Language': 'en-US',
                'Accept': 'application/json, text/javascript, */*; q=0.01',
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                'Origin': 'https://www.gigabyte.com',
                'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36',
                'X-Requested-With': 'XMLHttpRequest',
            }

            body = 'ck={}&f=0&page={}'.format(ck, page)

            yield scrapy.Request(url, method='POST', headers=headers, body=body, meta={'ck': ck}, callback=self.parse, dont_filter=True)


    def parse_item(self, response):
        try:
            data = json.loads(response.text)
        except ValueError as e:
            self.logger.warning('Unable to parse json at <{}>: {}'.format(response.url, response.text))
            return

        d = response.meta['item']

        d['specs'] = {}

        for l in data['ProductSpecList']:
            for s in l['ProductSpecData']:
                name = s['Name']
                value = [v.strip() for v in re.split('(<\s*/?br.*?>|\n)', s['Description'], flags=re.IGNORECASE)]
                value = [re.sub('^\s*-\s*', '', v) for v in value]
                value = [self.get_text(v) for v in value]
                value = list(filter(None, value))

                d['specs'][name] = value

        yield d


    def get_text(self, html):
        soup = BeautifulSoup(html, 'lxml')

        for tag in soup.find_all('sup'):
            tag.decompose()

        #text = soup.get_text(strip=True)
        text = soup.get_text()

        # text = re.sub('[\xa0 ]+', ' ', text)
        # text = text.strip()

        return text
