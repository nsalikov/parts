# -*- coding: utf-8 -*-
import re
import json
import scrapy
from urllib.parse import unquote


class LenovoSpider(scrapy.Spider):
    name = 'lenovo'
    allowed_domains = ['psref.lenovo.com']
    start_urls = ['http://psref.lenovo.com/']


    def parse(self, response):
        items_css = 'div#productlist_main a ::attr(href)'
        items = [re.sub('\?.*', '', response.urljoin(url)) for url in response.css(items_css).extract() if url.startswith('/Product/')]
        items = sorted(list(set(items)))

        for item in items:
            yield response.follow(item, callback=self.parse_item)


    def parse_item(self, response):
        data_css = 'input#hidJsonData ::attr(value)'
        data_text = unquote(response.css(data_css).extract_first())

        try:
            data = json.loads(data_text)
        except ValueError as e:
            self.logger.warning('Unable to parse json at <{}>: {}'.format(response.url, response.text))
            return

        fields = ['Machine Type', 'Processor', 'Memory', 'Storage Controller', 'Disk Bays', 'Disks', 'Optical', 'Add-on NIC', 'Slots', 'TPM', 'Operating System', 'Warranty', 'Power Supply']

        for product in data['ProductList']:
            match = re.search("\?M=(.+?)'", product['Model'])
            if match:
                model = match.group(1)

                d = {}

                d['url'] = response.url + '?M={}'.format(model)
                d['name'] = product['Product']
                d['manufactuer'] = 'Lenovo'
                d['series'] = None
                d['sku'] = model

                for f in fields:
                    if f in product:
                        d[f] = product[f]

                yield d
