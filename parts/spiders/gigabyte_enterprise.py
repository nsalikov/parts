# -*- coding: utf-8 -*-
import re
import json
import scrapy
from bs4 import BeautifulSoup
from scrapy.shell import inspect_response


class GigabyteEnterpriseSpider(scrapy.Spider):
    name = 'gigabyte_enterprise'
    allowed_domains = ['gigabyte.com']
    start_urls = [
        'https://www.gigabyte.com/R-Series',
        'https://www.gigabyte.com/H-Series',
        'https://www.gigabyte.com/G-Series',
        'https://www.gigabyte.com/E-Series',
        'https://www.gigabyte.com/S-Series',
        'https://www.gigabyte.com/W-Series',
        'https://www.gigabyte.com/OCP-Series'
    ]

    api_url = 'https://www.gigabyte.com/Ajax/Product/GetProductLineEnterPriseListByAjax'
    api_headers = {
            'accept-language': 'en-US,en;q=0.5',
            'accept': '*/*',
            'authority': 'www.gigabyte.com',
            'content-type': 'application/json',
            'origin': 'https://www.gigabyte.com',
            'referer': 'https://www.gigabyte.com',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36',
            'x-requested-with': 'XMLHttpRequest',
    }
    api_payload = '{"ActionUrl":"%s","ClassKey":"%s","FilterId":[""],"SelectPIds":[%s],"SortType":"new","nowPage":"%s"}'


    def start_requests(self):
        for url in self.start_urls:
            yield scrapy.Request(url, callback=self.parse, dont_filter=True)


    def parse(self, response):
        # inspect_response(response, self)

        action_url, class_key, ids, pages = self.get_filters(response)

        for page in pages:
            payload = self.api_payload % (action_url, class_key, ids, page)
            yield scrapy.Request(self.api_url, method='POST', headers=self.api_headers, body=payload, meta={'series': action_url}, callback=self.parse_ajax, dont_filter=True)


    def parse_ajax(self, response):
        # inspect_response(response, self)

        items = response.css('.ResultItemList .ResultItemRow a ::attr(href)').extract()

        for item in items:
            # self.logger.info('{} {}'.format(response.meta['series'], item))
            yield response.follow(item, meta={'series': response.meta['series']}, callback=self.parse_item)


    def parse_item(self, response):
        # inspect_response(response, self)

        pid = response.css('.body-content #isPid ::attr(value)').extract_first()

        if not pid:
            self.logger.warning('Unable to find PID at <>'.format(response.url))
            return

        url = 'https://www.gigabyte.com/api/ProductSpec/{}'.format(pid)

        headers = {
            'accept-language': 'en-US,en;q=0.5',
            'accept': 'application/json, text/javascript, */*; q=0.01',
            'authority': 'www.gigabyte.com',
            'referer': 'https://www.gigabyte.com',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36',
            'x-requested-with': 'XMLHttpRequest',
        }

        d = {}

        d['id'] = pid
        d['url'] = response.url
        d['name'] = ' '.join(filter(None, [re.sub('[\n\r\t\xa0 ]+', ' ', name, flags=re.DOTALL).strip() for name in response.css('h1.pageTitle ::text').extract()]))
        d['series'] = response.meta['series']
        d['breadcrumbs'] = [b.strip() for b in response.css('.breadcrumb li[itemprop="itemListElement"] span[itemprop="name"] ::text').extract()]

        yield scrapy.Request(url, headers=headers, callback=self.parse_item_ajax, meta={'item': d})


    def parse_item_ajax(self, response):
        try:
            data = json.loads(response.text)
        except ValueError as e:
            self.logger.warning('Unable to parse json at <{}>'.format(response.url))
            return

        d = response.meta['item']
        d['specs'] = {}

        for l in data['ProductSpecList']:
            for s in l['ProductSpecData']:
                name = s['Name']
                value = [v.strip() for v in re.split('(<\s*/?br.*?>|\n)', s['Description'], flags=re.IGNORECASE)]
                value = [re.sub('^\s*-\s*', '', v) for v in value]
                value = [self.get_text(v) for v in value]
                value = list(filter(None, value))

                d['specs'][name] = value

        yield d


    def get_filters(self, response):
        match = re.search('var dataObject = JSON\.stringify\(({[^}]+})\);', response.text, re.DOTALL)
        d = match.group(1)

        action_url = re.search("'ActionUrl'\s*:\s*'([^']+)'", d).group(1)
        if not action_url:
            action_url = ''

        class_key = re.search("'ClassKey'\s*:\s*'([^']+)'", d).group(1)
        if not class_key:
            class_key = ''

        ids = response.css('[name=FilterTypeCheckItem] ::attr(data-pid)').extract_first()
        if ids:
            ids = ','.join(['"{}"'.format(i.strip()) for i in ids.split(',')])
        else:
            ids = ''

        pages = response.css('.List-Paging .PagingArea .PagingItem .Paging ::attr(title)').extract()
        if not pages:
            pages = ['1']

        return action_url, class_key, ids, pages


    def get_text(self, html):
        soup = BeautifulSoup(html, 'lxml')

        for tag in soup.find_all('sup'):
            tag.decompose()

        #text = soup.get_text(strip=True)
        text = soup.get_text()

        # text = re.sub('[\xa0 ]+', ' ', text)
        # text = text.strip()

        return text
